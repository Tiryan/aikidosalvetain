# AikidoSalvetain

Refonte du site web de l'Aikido Salvetain

A faire : 

* Choix techniques
* Cahier des charges
* Enchainement d'écrans
* Mise en place du patron MVC
* Réalisation de la partie vue
* Vérification de la partie vue
* Réalisation des parties modèle et controleur
* Vérification et test d'intégration